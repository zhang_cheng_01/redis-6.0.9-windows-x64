# redis-6.0.9-windows-x64

#### 介绍
redis6.0.9的windows64位编译版本

#### 使用说明
直接运行redis-server.exe，配置为默认配置

需要配置文件生效请使用CMD运行

    redis-server.exe redis.conf

这样就可以带配置文件运行了

将redis根目录路径配置进系统变量PATH，就可以不需要进到redis的根目录下运行了

编译时间：2020-12-08

编译工具：Cygwin

redis版本：6.0.9

来源：https://redis.io/

